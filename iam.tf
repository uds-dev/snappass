resource "aws_iam_role" "ecs_service" {
  name = "snappass_ecs_role"

  assume_role_policy = <<EOF
{
  "Version": "2008-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ecs_service" {
  name = "snappass_ecs_policy"
  role = aws_iam_role.ecs_service.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "ec2:Describe*",
        "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
        "elasticloadbalancing:DeregisterTargets",
        "elasticloadbalancing:Describe*",
        "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
        "elasticloadbalancing:RegisterTargets"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

data "template_file" "ecs_profile" {
  template = "${file("${path.module}/iam-policy/fargate.json")}"

  vars = {
    log_group_app_arn = aws_cloudwatch_log_group.snappass_app.arn
    log_group_redis_arn  = aws_cloudwatch_log_group.snappass_redis.arn
  }
}

resource "aws_iam_role" "ecs_task" {
  name = "snappass_ecs_task_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "ecs" {
  name   = "snappass-ecs-policy"
  role   = aws_iam_role.ecs_task.name
  policy = data.template_file.ecs_profile.rendered
}

