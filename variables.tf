variable "acm_certificate" {
  description = "ACM Certificate for TLS termination in ALB"
  type        = string
}

variable "aws_ecs_cluster_id" {
  description = "Deploy snappass in the provided ECS cluster"
  type        = string
}

variable "app_container_cpu" {
  description = "cpu usage of app's container"
  type        = number
  default     = 128
}

variable "app_container_memory" {
  description = "memory usage of app's container"
  type        = number
  default     = 256
}

variable "aws_region" {
  description = "region to deploy in"
  type        = string
}

variable "redis_container_cpu" {
  description = "cpu usage of redis container"
  type        = number
  default     = 128
}

variable "redis_container_memory" {
  description = "memory usage of redis container"
  type        = number
  default     = 256
}

variable "snappass_desired_count_app" {
  description = "How many tasks? Each task is snappass+redis"
  type        = number
  default     = 1
}

variable "snappass_env_no_ssl" {
  description = "Set to '' if you want to set SSL, default is true (no ssl), because we terminate SSL on the ALB"
  type        = string
  default     = "True"
}

variable "snappass_docker_image" {
  description = "Docker image you want to use as the snappass app"
  type        = string
  default     = "unfor19/snappass"
}

variable "task_cpu" {
  description = "cpu usage of ecs task"
  type        = number
  default     = 256
}

variable "task_memory" {
  description = "memory usage of ecs task"
  type        = number
  default     = 512
}

variable "vpc_id" {
  description = "VPC ID that this app will be deployed to"
  type        = string
}

variable "vpc_cidr_ab" {
  description = "VPC CIDR AB, examples: 10.0"
  type        = string
  default     = "172.1"
}

variable "vpc_subnets_private_ids" {
  description = "Private Subnets list of ECS tasks - these tasks can receive traffic only from ALB"
  type        = list(string)
}

variable "vpc_subnets_public_ids" {
  description = "Public Subnets for ALB"
  type        = list(string)
}
