resource "aws_cloudwatch_log_group" "snappass_app" {
  name = "snappass/app"
}

resource "aws_cloudwatch_log_group" "snappass_redis" {
  name = "snappass/redis"
}
