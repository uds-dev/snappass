resource "aws_alb_target_group" "snappass_app" {
  name        = "snappass-app"
  port        = 5000
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip" # with networkmode awsvpc

  health_check {
    path                = "/"
    matcher             = "200"
    timeout             = "5"
    healthy_threshold   = "3"
    unhealthy_threshold = "3"
  }
}

resource "aws_alb" "front" {
  name            = "snappass-front-alb"
  internal        = false
  security_groups = ["${aws_security_group.web.id}"]
  subnets         = var.vpc_subnets_public_ids

  enable_deletion_protection = false

  tags = {
    Name = "snappass"
  }
}

resource "aws_alb_listener" "front_end_80" {
  load_balancer_arn = aws_alb.front.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_alb_listener" "front_end_443" {
  load_balancer_arn = aws_alb.front.arn
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = var.acm_certificate
  ssl_policy        = "ELBSecurityPolicy-2016-08"

  default_action {
    target_group_arn = aws_alb_target_group.snappass_app.arn
    type             = "forward"
  }
}
