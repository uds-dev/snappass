[
  {
    "name": "snappass-app",
    "image": "${app_image}",
    "cpu": ${app_container_cpu},
    "memory": ${app_container_memory},
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-stream-prefix": "snappass-app",
        "awslogs-group": "${log_group_app}",
        "awslogs-region": "${log_group_region}"
      }
    },
    "portMappings": [
      {
        "containerPort": 5000,
        "protocol": "tcp"
      }
    ],
    "environment": [
      {
        "name": "REDIS_HOST",
        "value": "localhost"
      },
      {
        "name": "NO_SSL",
        "value": "${no_ssl}"
      }
    ],
    "dependsOn": [
      {
        "condition": "HEALTHY",
        "containerName": "snappass-redis"
      }
    ]
  },
  {
    "name": "snappass-redis",
    "image": "redis:5.0.9",
    "cpu": ${redis_container_cpu},
    "memory": ${redis_container_memory},
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-stream-prefix": "snappass-redis",
        "awslogs-group": "${log_group_redis}",
        "awslogs-region": "${log_group_region}"
      }
    },
    "portMappings": [
      {
        "containerPort": 6379,
        "protocol": "tcp"
      }
    ],
    "healthCheck": {
      "command": [ "CMD-SHELL", "echo HEALTHY || exit 1" ]
    }
  }
]
