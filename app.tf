data "template_file" "snappass_app_task_definition" {
  template = file("${path.module}/task-definitions/app.json.tpl")

  vars = {
    log_group_region       = var.aws_region
    log_group_app          = aws_cloudwatch_log_group.snappass_app.name
    log_group_redis        = aws_cloudwatch_log_group.snappass_redis.name
    app_container_cpu      = var.app_container_cpu
    app_container_memory   = var.app_container_memory
    app_image              = var.snappass_docker_image
    redis_container_cpu    = var.redis_container_cpu
    redis_container_memory = var.redis_container_memory
    no_ssl                 = var.snappass_env_no_ssl
  }
}

resource "aws_ecs_task_definition" "snappass_app" {
  family                = "snappass-app"
  container_definitions = data.template_file.snappass_app_task_definition.rendered
  network_mode          = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  
  execution_role_arn = aws_iam_role.ecs_task.arn
  task_role_arn      = aws_iam_role.ecs_task.arn

  cpu    = var.task_cpu
  memory = var.task_memory

}

resource "aws_ecs_service" "snappass_app" {
  name            = "snappass-app"
  cluster         = var.aws_ecs_cluster_id
  desired_count   = var.snappass_desired_count_app
  task_definition = aws_ecs_task_definition.snappass_app.arn
  launch_type     = "FARGATE"

  load_balancer {
    target_group_arn = aws_alb_target_group.snappass_app.id
    container_name   = "snappass-app"
    container_port   = "5000"
  }

  network_configuration {
    security_groups = ["${aws_security_group.snappass_app.id}"]
    subnets         = var.vpc_subnets_private_ids
  }

  depends_on = [
    aws_alb_listener.front_end_80,
    aws_alb_listener.front_end_443,
  ]

}
